# Thank you Juan and Scott!

## What is something you appreciate about Juan?

Juan trusts his team to make the right call, even when he doesn't completely agree. 

Juan is a hallmark optimist. He sees the best in every person he meets and every situation he encounters, and never leaves a room or call without elevating and inspiring those around him.

## What is something you appreciate about Scotty?

Scotty is not afraid to step up and make the hard decisions when warranted.

Scotty looks out for his team. I'm grateful that he was willing to take a chance on me as a n00b engineer, and always goes the extra mile to make sure the team has the resources needed to succeed.
